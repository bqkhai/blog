import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    // role: {},
    email: {
        type: String,
        // unique: true,
        // required: true,
    },
    name: {
        type: String,
    },
    password: {
        type: String,
        // required: true,
    },
});

export const User = mongoose.model('User', UserSchema);
