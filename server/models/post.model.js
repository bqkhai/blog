import mongoose from 'mongoose';

const PostSchema = mongoose.Schema(
    {
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },

        title: {
            type: String,
        },
        content: {
            type: String,
        },
        imageURL: {
            type: String,
        },
    },
    { timestamps: true }
);

export const Post = mongoose.model('Post', PostSchema);
