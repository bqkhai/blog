import jwt from 'jsonwebtoken';

const verifyToken = async (req, res, next) => {
    const authHeader = req.header('Authorization');
    const token = authHeader && authHeader.split(' ')[1];

    if (!token) {
        return res.status(401).json({ message: 'Access denined' });
    }

    try {
        const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
        console.log(decoded);
        req.user = decoded;
        next();
    } catch (err) {
        console.log(err);
        return res.status(403).json({ message: 'Invalid Token' });
    }
};

export default verifyToken;
