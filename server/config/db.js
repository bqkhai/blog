import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();
const db_url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.mvpzx.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

const ConnectDB = async () => {
    try {
        await mongoose.connect(db_url);
        // console.log(process.env.DB_NAME);
        console.log('Connected to DB');
    } catch (error) {
        console.log(error);
    }
};

export default ConnectDB;
