import { User } from '../models/user.model.js';
import { Post } from '../models/post.model.js';

import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
dotenv.config();

const signup = async (req, res) => {
    try {
        // Check field is empty
        if (!req.body.name || !req.body.email || !req.body.password) {
            return res.status(400).json({
                message: 'Missing name, email or password',
            });
        }
        // Check email exist
        const checkUserByEmail = await User.findOne({
            email: req.body.email,
        });
        if (!checkUserByEmail) {
            // Create new user
            const newUserData = {
                ...req.body,
            };
            // Hass password
            const hassPassword = await bcrypt.hash(req.body.password, 10);
            newUserData.password = hassPassword;

            const newUser = await User.create(newUserData);
            const accessToken = jwt.sign(
                { user_id: newUser._id, username: newUser.name },
                process.env.TOKEN_SECRET
            );

            return res
                .status(201)
                .json({ message: 'Created success', token: accessToken });
        }
        res.status(400).json({
            message: 'Email is already',
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Created failed' });
    }
};

// @controller Login
const login = async (req, res) => {
    try {
        const { email, password } = req.body;

        // Check validate email and password
        if (!email || !password) {
            return res.status(400).json({
                success: false,
                message: 'Missing email or password',
            });
        }
        // Find user by email
        const user = await User.findOne({
            email: req.body.email,
        });
        // If not exist user return false
        if (!user) {
            return res
                .status(400)
                .json({ message: 'Invalid email or password' });
        }

        // Compare password
        const validPassword = await bcrypt.compare(
            req.body.password,
            user.password
        );
        // Compare password is false
        if (!validPassword) {
            return res.status(400).json({
                message: 'Invalid email or password',
            });
        }

        // Email and password is valid => login true and return token
        const accessToken = jwt.sign(
            { user_id: user._id, username: user.name },
            process.env.TOKEN_SECRET
        );

        res.cookie('token', accessToken);

        return res.status(200).json({
            message: 'Login success',
            token: accessToken,
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            message: 'Login failed',
        });
    }
};

export default {
    signup,
    login,
};
