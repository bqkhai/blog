import { User } from '../models/user.model.js';
import { Post } from '../models/post.model.js';

const getAllPosts = async (req, res) => {
    try {
        const posts = await Post.find();
        res.status(200).json(posts);
    } catch (error) {
        console.log(error);
        res.status(500).json('Server error');
    }
};

const getPostById = async (req, res) => {
    try {
        const post_id = req.params.id;
        const post = await Post.findById(post_id);
        res.status(200).json(post);
    } catch (error) {
        console.log(error);
        res.status(500).json('Server error');
    }
};

const getPostByUserID = async (req, res) => {
    try {
        console.log(req.user);
        const posts = await Post.find({ user_id: req.user.user_id });
        res.status(200).json(posts);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: error.message });
    }
};

const createPost = async (req, res) => {
    try {
        const postData = {
            ...req.body,
            user_id: req.user.user_id,
        };
        const newPost = new Post(postData);
        await newPost.save();
        res.status(201).json('Created success');
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: error.message });
    }
};

const editPost = async (req, res) => {
    try {
        const post_id = req.params.id;
        const updateData = {
            ...req.body,
            user_id: req.user.user_id,
        };
        await Post.findByIdAndUpdate(post_id, updateData);
        res.status(204).json('Updated success');
    } catch (error) {
        console.log(error);
        res.status(500).json('Server error');
    }
};

const delPost = async (req, res) => {
    try {
        const post_id = req.params.id;
        await Post.findByIdAndDelete({
            _id: post_id,
            user_id: req.user.user_id,
        });
        res.status(204).json('Deleted success');
    } catch (error) {
        console.log(error);
        res.status(500).json('Server error');
    }
};

export default {
    getAllPosts,
    getPostById,
    getPostByUserID,
    createPost,
    editPost,
    delPost,
};
