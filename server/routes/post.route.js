import express from 'express';
import postController from '../controllers/post.controller.js';
import authMiddleware from '../middlewares/auth.middleware.js';

const router = express.Router();

// public route
router.get('/', postController.getAllPosts);
// public route
router.get('/:id', postController.getPostById);
// private route
router.get('/manage', authMiddleware, postController.getPostByUserID);
// private route
router.post('/create', authMiddleware, postController.createPost);
// private route
router.put('/:id', authMiddleware, postController.editPost);
// private route
router.delete('/:id', authMiddleware, postController.delPost);

export default router;
