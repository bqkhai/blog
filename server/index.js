import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import ConnectDB from './config/db.js';
import postRoutes from './routes/post.route.js';
import userRoutes from './routes/user.route.js';

dotenv.config();

const app = express();
const PORT = process.env.PORT || 5000;

app.use(
    cors({
        origin: process.env.FE_HOST,
        credentials: true,
    })
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

ConnectDB();

app.use('/user', userRoutes);
app.use('/post', postRoutes);

app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
});
