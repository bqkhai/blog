import Link from 'next/link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { Footer } from '../components/footer/footer';
import { Header } from '../components/header/header';
import style from '../styles/form.module.css';
import instance from '../config/config';
import Head from 'next/head';

const Signup = () => {
    const router = useRouter();

    const [data, setData] = useState({
        email: '',
        name: '',
        password: '',
        cf_pass: '',
    });

    const handleChange = (e: any) => {
        const value = e.target.value;

        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();

        const dataUser = {
            email: data.email,
            name: data.name,
            password: data.password,
        };

        if (data.password === data.cf_pass) {
            instance
                .post(`/user/signup`, dataUser)
                .then((res) => {
                    console.log(res.data);
                    alert('Đăng ký thành công. Hãy đăng nhập!');
                    router.push('/login');
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    return (
        <div className={style.wrapper}>
            <Head>
                <title>Đăng ký</title>
            </Head>
            <Header />
            <div className='container'>
                <div className={style.form_wrapper}>
                    <form className={style.form_signup} onSubmit={handleSubmit}>
                        <div className={style.form_item}>
                            <label>Email</label>
                            <input
                                className={style.input}
                                name='email'
                                value={data.email}
                                onChange={handleChange}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label>Tên</label>
                            <input
                                className={style.input}
                                name='name'
                                value={data.name}
                                onChange={handleChange}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label>Mật khẩu</label>
                            <input
                                className={style.input}
                                name='password'
                                value={data.password}
                                onChange={handleChange}
                                type='password'
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label>Nhập lại mật khẩu</label>
                            <input
                                className={style.input}
                                name='cf_pass'
                                value={data.cf_pass}
                                onChange={handleChange}
                                type='password'
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <button className={style.form_btn}>Đăng ký</button>
                        </div>
                        <div className={style.redirect_page}>
                            <Link href='/login'>
                                Đã có tài khoản. Đăng nhập
                            </Link>
                        </div>
                    </form>
                </div>
            </div>

            <Footer />
        </div>
    );
};

export default Signup;
