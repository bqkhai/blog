import { useState, useEffect } from 'react';
import Image from 'next/image';
import Head from 'next/head';
import Link from 'next/link';

import { Header } from '../components/header/header';
import { NextPageContext } from 'next';
import style from '../styles/home.module.css';
import { Loading } from '../components/loading/loading';
import { Footer } from '../components/footer/footer';
import AddButton from '../components/add-button/add-button';
import instance from '../config/config';

interface IPost {
    _id: string;
    title: string;
    content: string;
    imageURL: string;
}

interface IProps {
    posts: IPost[];
}

export default function Home({ posts: serverPosts }: IProps) {
    const [posts, setPosts] = useState(serverPosts);

    useEffect(() => {
        async function load() {
            await instance
                .get(`/post`)
                .then((res) => {
                    setPosts(res.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        }

        if (!serverPosts) {
            load();
        }
    }, []);

    if (!posts || posts === null) {
        return <Loading />;
    }

    function PostCard() {
        return posts?.map((post, idx) => {
            return (
                <Link href='/posts/[id]' as={`/posts/${post._id}`} key={idx}>
                    <div className={style.card}>
                        <Image
                            alt='Mountains'
                            src={`${post.imageURL}`}
                            layout='fill'
                            objectFit='cover'
                            quality={100}
                        />
                        <div className={style.post_title}>
                            <p>{post.title}</p>
                        </div>
                    </div>
                </Link>
            );
        });
    }

    return (
        <div>
            <Head>
                <title>Trang chủ</title>
            </Head>
            <Header />
            <AddButton />
            <div className={style.wrapper}>
                <div className='container'>
                    <div className={style.post_wrapper}>
                        {posts ? PostCard() : null}
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

Home.getInitialProps = async (ctx: NextPageContext) => {
    if (!ctx.req) {
        return { posts: null };
    }
    const response = await instance.get(`/post`);
    const posts = await response.data;
    return {
        posts,
    };
};
