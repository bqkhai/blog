import { Footer } from '../components/footer/footer';
import { Header } from '../components/header/header';
import Link from 'next/link';
import style from '../styles/404.module.css';

const PageNotFound = () => {
    return (
        <div>
            <Header />
            <div className='container'>
                <div className={style.pagenotfound}>
                    <div className={style.text_notfound}>
                        <p>404 | Page not found</p>
                    </div>
                    <div className={style.link}>
                        <Link href='/'>Về trang chủ</Link>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default PageNotFound;
