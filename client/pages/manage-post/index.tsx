import { useEffect, useState } from 'react';
import { Footer } from '../../components/footer/footer';
import { Header } from '../../components/header/header';
import Link from 'next/link';
import style from '../../styles/table.module.css';
import { useRouter } from 'next/router';
import instance from '../../config/config';
import Head from 'next/head';
import Cookies from 'js-cookie';

const ManagePost = () => {
    const [data, setData] = useState([]);
    const router = useRouter();

    useEffect(() => {
        const token = Cookies.get('token');
        if (!token) {
            alert('Bạn cần đăng nhập để làm điều này');
            router.push('/');
        } else {
            instance
                .get('/post')
                .then((res) => {
                    console.log(res.data);
                    setData(res.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }, []);

    const deletePost = (id: any) => {
        if (confirm('Bạn có chắc muốn xóa không')) {
            console.log(id);
            instance
                .delete(`/post/${id}`)
                .then(() => {
                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    const PostList = () => {
        return data.map((post: any, index) => {
            return (
                <tr key={index} className={`${style.row} ${style.tr}`}>
                    <td className={style.title}>{post.title}</td>
                    <td className={style.imageURL}>{post.imageURL}</td>
                    <td className={style.content}>{post.content}</td>
                    <td className={style.td_action}>
                        <div className={style.edit}>
                            <Link href={`/manage-post/${post._id}`}>Sửa |</Link>
                        </div>
                        <div className={style.edit}>
                            <p onClick={() => deletePost(post._id)}>Xóa</p>
                        </div>
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <Head>
                <title>Quản lý</title>
            </Head>
            <Header />
            <div className='container'>
                <table className={style.table}>
                    <thead className={`${style.row} ${style.thead}`}>
                        <tr>
                            <th className={`${style.th_title}`}>Tiêu đề</th>
                            <th className={style.th}>URL ảnh</th>
                            <th className={style.th}>Nội dung</th>
                            <th className={style.th_action}>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>{PostList()}</tbody>
                </table>
            </div>
            <Footer />
        </div>
    );
};

export default ManagePost;
