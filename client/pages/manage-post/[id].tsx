import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Footer } from '../../components/footer/footer';
import { Header } from '../../components/header/header';
import style from '../../styles/form.module.css';
import instance from '../../config/config';
import Head from 'next/head';

const PostDetail = () => {
    // const [data, setData] = useState({
    //     title: '',
    //     content: '',
    //     imageURL: '',
    // });

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [imageURL, setImageURL] = useState('');

    // const handleChange = (e) => {
    //     const value = e.target.value;
    //     setData({
    //         ...data,
    //         [e.target.name]: value,
    //     });
    // };

    const router = useRouter();
    const id = router.query.id;

    useEffect(() => {
        instance
            .get(`/post/${id}`)
            .then((res) => {
                console.log(res.data);
                setTitle(res.data.title);
                setContent(res.data.content);
                setImageURL(res.data.imageURL);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [id]);

    const updatePost = async () => {
        const data = { title, content, imageURL };

        await instance
            .put(`/post/${id}`, data)
            .then(() => router.push('/manage-post'))
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <div className={style.wrapper}>
            <Head>
                <title>Chỉnh sửa</title>
            </Head>
            <Header />
            <div className='container'>
                <div className={style.form_wrapper}>
                    <form
                        className={style.form}
                        onSubmit={(e) => e.preventDefault()}
                    >
                        <Link href='/manage-post'>
                            <div className={style.btnBack}>
                                <Image
                                    src='/static/back2.svg'
                                    width={15}
                                    height={15}
                                ></Image>
                                Quay lại
                            </div>
                        </Link>
                        <div className={style.form_item}>
                            <label className={style.label}>Tiêu đề</label>
                            <input
                                className={style.input}
                                onChange={(e) => setTitle(e.target.value)}
                                value={title}
                                spellCheck='false'
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label className={style.label}>Nội dung</label>
                            <textarea
                                className={style.textarea}
                                onChange={(e) => setContent(e.target.value)}
                                value={content}
                                spellCheck='false'
                            ></textarea>
                        </div>
                        <div className={style.form_item}>
                            <label className={style.label}>URL ảnh</label>
                            <input
                                className={style.input}
                                onChange={(e) => setImageURL(e.target.value)}
                                value={imageURL}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <button
                                className={style.form_btn}
                                onClick={updatePost}
                            >
                                Gửi
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <Footer />
        </div>
    );
};

export default PostDetail;
