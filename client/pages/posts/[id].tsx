import { useEffect, useState } from 'react';
import { NextPageContext } from 'next';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';

import { Header } from '../../components/header/header';
import { PostCard } from '../../components/post-card/post-card';
import { Loading } from '../../components/loading/loading';

import style from '../../styles/post-detail.module.css';
import instance from '../../config/config';

interface IPost {
    _id: string;
    title: string;
    content: string;
    imageURL: string;
}

interface Iprop {
    post: IPost;
}

export default function PostDetail({ post: serverPost }: Iprop) {
    const [post, setPost] = useState(serverPost);

    const router = useRouter();
    const { id } = router.query;

    useEffect(() => {
        async function load() {
            await instance
                .get(`/post/${id}`)
                .then((res) => {
                    setPost(res.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        if (!serverPost) {
            load();
        }
    }, []);

    if (!post) {
        return <Loading />;
    }

    return (
        <div className={style.wrapper}>
            <Head>
                <title>Post: {post.title}</title>
            </Head>

            <Header />
            <div className='container'>
                <Link href='/'>
                    <button className={style.btnBack}>
                        <Image src='/static/back2.svg' width={20} height={15} />
                        Quay lại
                    </button>
                </Link>

                <div>
                    <PostCard
                        imageURL={post.imageURL + `?sig=${post._id}`}
                        title={post.title}
                        content={post.content}
                    />
                </div>
            </div>
        </div>
    );
}

PostDetail.getInitialProps = async ({ query, req }: NextPageContext) => {
    if (!req) {
        return { post: null };
    }

    const response = await instance.get(`/post/${query.id}`);
    const post = await response.data;
    return {
        post,
    };
};
