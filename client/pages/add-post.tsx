import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { Footer } from '../components/footer/footer';
import { Header } from '../components/header/header';
import style from '../styles/form.module.css';
import instance from '../config/config';
import Cookies from 'js-cookie';

const AddPost = () => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [imageURL, setImageURL] = useState('');
    const router = useRouter();

    const addPost = async () => {
        const token = Cookies.get('token');
        if (!token) {
            alert('Bạn cần đăng nhập để làm điều này');
            router.push('/');
        } else {
            const data = { title, content, imageURL };
            await instance
                .post('/post/create', data)
                .then(() => router.push('/'))
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    return (
        <div className={style.wrapper}>
            <Header />
            <div className='container'>
                <div className={style.form_wrapper}>
                    <form
                        className={style.form}
                        onSubmit={(e) => e.preventDefault()}
                    >
                        <Link href='/'>
                            <div className={style.btnBack}>
                                <Image
                                    src='/static/back2.svg'
                                    width={15}
                                    height={15}
                                ></Image>
                                Quay lại
                            </div>
                        </Link>
                        <div className={style.form_item}>
                            <label className={style.label}>Tiêu đề</label>
                            <input
                                className={style.input}
                                onChange={(e) => setTitle(e.target.value)}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label className={style.label}>Nội dung</label>
                            <textarea
                                className={style.textarea}
                                onChange={(e) => setContent(e.target.value)}
                            ></textarea>
                        </div>
                        <div className={style.form_item}>
                            <label className={style.label}>URL ảnh</label>
                            <input
                                className={style.input}
                                onChange={(e) => setImageURL(e.target.value)}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <button
                                className={style.form_btn}
                                onClick={addPost}
                            >
                                Gửi
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <Footer />
        </div>
    );
};

export default AddPost;
