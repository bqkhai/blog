import { Footer } from '../../components/footer/footer';
import { Header } from '../../components/header/header';

const Profile = () => {
    return (
        <div>
            <Header />
            <div className='container'>Profile</div>
            <Footer />
        </div>
    );
};

export default Profile;
