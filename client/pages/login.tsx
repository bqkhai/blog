import Link from 'next/link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { Footer } from '../components/footer/footer';
import { Header } from '../components/header/header';
import style from '../styles/form.module.css';
import instance from '../config/config';
import Head from 'next/head';

const Login = () => {
    const router = useRouter();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const Login = async () => {
        const data = { email, password };
        await instance
            .post(`/user/login`, data)
            .then((res) => {
                console.log(res.data);
                router.push('/');
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('isLogged', 'true');
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <div className={style.wrapper}>
            <Head>
                <title>Đăng nhập</title>
            </Head>
            <Header />
            <div className='container'>
                <div className={style.form_wrapper}>
                    <form
                        className={style.form_login}
                        onSubmit={(e) => e.preventDefault()}
                    >
                        <div className={style.form_item}>
                            <label>Email</label>
                            <input
                                className={style.input}
                                onChange={(e) => setEmail(e.target.value)}
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <label>Mật khẩu</label>
                            <input
                                className={style.input}
                                onChange={(e) => setPassword(e.target.value)}
                                type='password'
                            ></input>
                        </div>
                        <div className={style.form_item}>
                            <button className={style.form_btn} onClick={Login}>
                                Đăng nhập
                            </button>
                        </div>
                        <div className={style.redirect_page}>
                            <Link href='/signup'>
                                Chưa có tài khoản. Đăng ký
                            </Link>
                        </div>
                    </form>
                </div>
            </div>

            <Footer />
        </div>
    );
};

export default Login;
