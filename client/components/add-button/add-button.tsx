import Link from 'next/link';
import style from './add-button.module.css';

const AddButton = () => {
    return (
        <div className='container'>
            <div className={style.NavBarItem}>
                <Link href='/add-post'>
                    <button className={style.AddPostBtn}>Thêm mới</button>
                </Link>
            </div>
        </div>
    );
};

export default AddButton;
