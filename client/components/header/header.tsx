import Link from 'next/link';
import Image from 'next/image';
import style from './header.module.css';
import jwt from 'jsonwebtoken';
import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';

export const Header = () => {
    const [user, setUser] = useState({
        username: '',
        userID: '',
        iat: '',
        isLogged: false,
    });

    const [isOpenDropdown, setIsOpenDropdown] = useState(false);

    useEffect(() => {
        const token = Cookies.get('token');
        const data: any = jwt.decode(token!);
        console.log(data);
        if (data) {
            setUser({
                ...data,
                isLogged: true,
            });
        }
    }, []);

    const showDropdown = () => {
        setIsOpenDropdown(!isOpenDropdown);
    };

    const Logout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('isLogged');
        Cookies.remove('token');
        window.location.href = '/';
    };

    return (
        <nav className={style.Nav}>
            <div className={style.container}>
                <div className={style.NavBarItem}>
                    <Link href='/'>
                        <a className={style.logo}>Blog</a>
                    </Link>
                </div>
                <div className={style.NavBarItem}>
                    <Link href='/manage-post'>
                        <a className={style.logo}>Bài đăng</a>
                    </Link>
                </div>
                {user.isLogged === true ? (
                    <div>
                        <div className={`${style.NavBarItem} ${style.box}`}>
                            <a className={style.logout}>
                                Xin chào {user.username}
                            </a>
                            <div
                                className={style.dropdown}
                                onClick={showDropdown}
                            >
                                {isOpenDropdown === true ? (
                                    <Image
                                        src='/static/dropup.svg'
                                        width={12}
                                        height={12}
                                    ></Image>
                                ) : (
                                    <Image
                                        src='/static/dropdown.svg'
                                        width={12}
                                        height={12}
                                    ></Image>
                                )}
                            </div>
                        </div>
                        {isOpenDropdown === true ? (
                            <div className={style.NavBarItem}>
                                <div className={style.logout2} onClick={Logout}>
                                    Đăng xuất
                                </div>
                            </div>
                        ) : null}
                    </div>
                ) : (
                    <div className={style.NavBarItem}>
                        <Link href='/login'>
                            <a className={style.logout}>Đăng kí/ đăng nhập</a>
                        </Link>
                    </div>
                )}
            </div>
        </nav>
    );
};
