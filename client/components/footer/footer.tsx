import Link from 'next/link';
import style from './footer.module.css';

export const Footer = () => {
    return (
        <div className={style.footer}>
            <div className={style.text}>
                Copyright @
                <Link href='/'>
                    <a className={style.link}> My Blog </a>
                </Link>
                2022.
            </div>
        </div>
    );
};
