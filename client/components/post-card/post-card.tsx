import Image from 'next/image';
import style from './post-card.module.css';

interface Props {
    imageURL: string;
    title: string;
    content: string;
}

export const PostCard = ({ imageURL, title, content }: Props) => {
    return (
        <div className={style.post}>
            <h2>Bài đăng : {title}</h2>
            <div className={style.card}>
                <Image
                    alt='Card'
                    src={imageURL}
                    objectFit='cover'
                    quality={100}
                    width={700}
                    height={470}
                />
            </div>
            <div className={style.post_title}>
                <p> Nội dung: {content}</p>
            </div>
        </div>
    );
};
